import copy, random


def init(size):
    array = [[0] * size for i in range(size)]
    for i in range(size):
        for j in range(size):
            array[i][j] = " "
    return array

def print_array_head(size):
    print("     ", end="")
    for i in range(size):
        print(i, end=" ")
    print()
    print("     ", end="")
    for _ in range(size):
        print("-", end=" ")
    print()

def print_formatted_array(state):
    print_array_head(len(state))
    for i in range(len(state)):
        print(i, " | ", end="")
        for j in range(len(state)):
            print(state[i][j], end=" ")
        print("")


def perform_logic_direction(state, symbol, x, y, dx, dy):
    stream = True
    bordered = False
    esymbol = "X" if symbol == "O" else "O"
    i = 1
    while(y + (i * dy) >= 0 and y + (i * dy) < len(state) and x + (i * dx) >= 0 and x + (i * dx) < len(state)):
        if(stream and state[y + (i * dy)][x + (i * dx)] == esymbol):
            pass
        elif(stream and state[y + (i * dy)][x + (i * dx)] == symbol):
            stream = False
            bordered = True
        else:
            stream = False
        i += 1
    if(bordered):
        i = 1
        while(state[y + (i * dy)][x + (i * dx)] == esymbol):
            state[y + (i * dy)][x + (i * dx)] = symbol
            i += 1

def perform_logic(state, symbol, x, y):
    perform_logic_direction(state, symbol, x, y, 1, 0)
    perform_logic_direction(state, symbol, x, y, 0, 1)
    perform_logic_direction(state, symbol, x, y, -1, 0)
    perform_logic_direction(state, symbol, x, y, 0, -1)
    perform_logic_direction(state, symbol, x, y, 1, 1)
    perform_logic_direction(state, symbol, x, y, 1, -1)
    perform_logic_direction(state, symbol, x, y, -1, 1)
    perform_logic_direction(state, symbol, x, y, -1, -1)


def direction_stream(state, x, y):
    i = 0
    nextSymbol = True
    while((x - i >= 0 or x + i < len(state) or y - i >= 0 or y + i < len(state)) and nextSymbol):
        nextSymbol = False
        if(x - i - 1 >= 0 and state[y][x - i - 1] == state[y][x]):
            nextSymbol = True
        elif(y - i - 1 >= 0 and state[y - i - 1][x] == state[y][x]):
            nextSymbol = True
        elif(x + i + 1 < len(state) and state[y][x + i + 1] == state[y][x]):
            nextSymbol = True
        elif(y + i + 1 < len(state) and state[y + i + 1][x] == state[y][x]):
            nextSymbol = True
        elif(y - i - 1 >= 0 and x - i - 1 >= 0 and state[y - i - 1][x - i - 1] == state[y][x]):
            nextSymbol = True
        elif(y - i - 1 >= 0 and x + i + 1 < len(state) and state[y - i - 1][x + i + 1] == state[y][x]):
            nextSymbol = True
        elif(x + i + 1 < len(state) and y + i + 1 < len(state) and state[y + i + 1][x + i + 1] == state[y][x]):
            nextSymbol = True
        elif(y + i + 1 < len(state) and x - i - 1 >= 0 and state[y + i + 1][x - i - 1] == state[y][x]):
            nextSymbol = True
        i = i + 1
        if(i >= 5):
            return state[y][x]
    return " "



def check_win(state):
    for i in range(len(state)):
       for j in range(len(state)):
            if(state[j][i] == " "):
                pass
            else:
                currCoord = direction_stream(state, i, j)
                if(currCoord != " "):
                   return currCoord
    return " "


def strategy(state, mark):
    """
    Funkce, ktera pro zadany stav vrarati tah, ktery se ma zahrat.
    :param state: hraci plocha reprezentovana seznamen seznamu
    :param mark: znacka, kterou ma funkce umistit na herni plan
    :return: dvojice (radek, sloupc) popisujici tah.
    """
    for i in range(len(state)):
        for j in range(len(state)):
            if(state[i][j] != " "):
                pass
            else:
                tempstate = copy.deepcopy(state)
                tempstate[i][j] = mark
                perform_logic(tempstate, mark, i, j)
                if(check_win(tempstate) != " "):
                    return i, j
    col = random.randint(0, len(state) - 1)
    row = random.randint(0, len(state) - 1)
    while(state[row][col] != " "):
        col = random.randint(0, len(state) - 1)
        row = random.randint(0, len(state) - 1)
    return row, col


def play(size, human_starts=True):
    """
    Funkce pro simulaci hry jednoho hrace s pocitacem.
    :param size: prirozene cislo vetsi rovno 5 udavajici velikost herniho planu
    :param human_starts: booleovsky prepinach udavajici, zda ma zacinat hrac
    """
    state = init(size)
    if(human_starts):
        print_formatted_array(state)
        x, y = map(int, input("Enter coordinates (x, y): ").split())
        while(state[y][x] != " "):
            x, y = map(int, input("Coordinate already taken, try again: ").split())
        state[y][x] = "O"
        perform_logic(state, "O", x, y)
    winning_character = " "
    while(winning_character == " "):
        x, y = strategy(state, "X")
        state[y][x] = "X"
        perform_logic(state, "X", x, y)
        if(check_win(state) == "X"):
            winning_character = "X"
        else:
            print_formatted_array(state)
            print("Enter coordinates (x, y): ")
            x, y = map(int, input().split())
            while(state[y][x] != " "):
                x, y = map(int, input("Coordinate already taken, try again: ").split())
            state[y][x] = "O"
            perform_logic(state, "O", x, y)
            winning_character = check_win(state)
    if(winning_character == "X"):
        print("Computer has won!")
    else:
        print("Player has won!")
    print_formatted_array(state)

play(9)